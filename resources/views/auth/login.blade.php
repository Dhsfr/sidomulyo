@extends('layouts.app')
@section('content')
<form method="POST" action='{{ url("login/$url") }}'>
    {{ csrf_field() }}
    <div class="form-group">
        <label class="sr-only" for="validationEmail">EmailAddress</label>
        <input class="form-control form-control-lg input" id="validationEmail" type="text" placeholder="EmailAddress" name="email">
    </div>
    <div class="form-group">
        <label class="sr-only" for="validationPassword">Password</label>
        <input class="form-control form-control-lg input" id="validationPassword" type="password" placeholder="Password" name="password">
    </div>
    <div class="container-login-form-btn">
        <button class="login-form-btnFacebook">
            <i class="fa fa-facebook-square" aria-hidden="true"> Login</i>
        </button>
    </div>
</form>
@endsection
