@extends('user.layouts.app')
@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('assets/images/profil/gangBali.JPG')}}');">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
            	<div class="col-md-9 ftco-animate text-center pt-md-5 pt-5">
                	<h1 class="mb-3 bread">Profil Desa</h1>
                	<p class="breadcrumbs"><span class="mr-2"><a href="{{route('user.home')}}">Beranda</a></span> <span>Profil Desa</span></p>
                </div>
            </div>
		</div>
	</div>	
</section>
<br>
<section class="ftco-appointment">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row d-md-flex align-items-center">
    			<div class="col-md-6 d-flex align-self-stretch img" style="background-image: url({{asset('assets/images/beranda/palkuning.JPG')}});"></div>
	    			<div class="col-md-6 appointment pl-md-5 py-md-5 ftco-animate">
                		<h3 class="mb-3">Batas Wilayah</h3>
                		<form action="#" class="appointment-form">
	    					<div class="form-group">
                    			<div class="d-flex">
                                    <p>Desa Sidomulyo merupakan daerah paling timur wilayah Kabupaten Jember yang berbatasan langsung dengan Kabupaten Banyuwangi. Adapun batas Desa Sidomulyo adalah sebagai berikut :</p>
                    			</div>
                                <div class="d-flex">
                                    Utara        : Desa Sumberjati
                                <br>
                                    Timur       : Desa Curahleduk, Kecamatan Kalibaru – Kabupaten Banyuwangi
                                <br>
                                    Selatan     : Desa Pace dan Desa Mulyorejo
                                <br>
                                    Barat        : Desa Garahan 
                                </div>
	    					</div>
                		</form>
	    			</div>    			
            	</div>
			</div>
		</div>
	</div>	
</section>
<!-- <section class="ftco-section bg-light">
	<div class="container">
    	<div class="row justify-content-center">
    		<div class="col-md-8 text-center">
            	<div class="heading-section mb-5">
                	<span class="subheading"><small><i class="left-bar"></i>Services<i class="right-bar"></i></small></span>
	            	<h2 class="mb-1">Kick your <span>feet</span> up</h2>
	          	</div>
    		</div>
        </div>
        <div class="row">
    		<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-ruler"></span>
					</div>
					<div class="text px-md-2">
                		<h3>Analyze Your Goal</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
    		</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-gym"></span>
					</div>
					<div class="text px-md-2">
                		<h3>Work Hard On It</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
			</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-tools-and-utensils"></span>
					</div>
					<div class="text px-md-2">
                		<h3>Improve Your Performance</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
			</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-abs"></span>
					</div>
					<div class="text px-md-2">
                		<h3>Achieve Your Perfect Body</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
			</div>
        </div>
    </div>
</section> -->
<!-- <section class="ftco-appointment">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row d-md-flex align-items-center">
    			<div class="col-md-6 d-flex align-self-stretch img" style="background-image: url({{asset('assets/images/about-3.jpg')}});"></div>
	    			<div class="col-md-6 appointment pl-md-5 py-md-5 ftco-animate">
                		<h3 class="mb-3">Calculate Your BMI</h3>
                		<form action="#" class="appointment-form">
	    					<div class="form-group">
                    			<label for="height">Height</label>
                    			<div class="d-flex">
		    						<input type="text" class="form-control mr-2" placeholder="feet">
		    						<input type="text" class="form-control ml-2" placeholder="inches">
                    			</div>
	    					</div>
	    					<div class="form-group">
                    			<div class="d-flex">
	    							<div class="mr-2">
                        				<label for="height">Weight</label>
                        				<input type="text" class="form-control" placeholder="lbs">
			    					</div>
		    						<div class="ml-2">
                        				<label for="height">Age</label>
                        				<input type="text" class="form-control" placeholder="yrs">
			    					</div>
                    			</div>
	    					</div>
	    					<div class="d-md-flex">
                            	<div class="form-group d-flex">
		            				<input type="submit" value="Calculate" class="btn btn-secondary py-3 px-4 mr-2">
                                	<input type="submit" value="BMI" class="btn btn-primary py-3 px-4 ml-2">
                            	</div>
	    					</div>
                		</form>
	    			</div>    			
            	</div>
			</div>
		</div>
	</div>	
</section> -->
<section class="ftco-section testimony-section">
	<div class="container">
    	<div class="row justify-content-center mb-5 pb-3">
        	<div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-1">Dusun Sidomulyo</h2>
            </div>
        </div>
        <div class="row ftco-animate">
        	<div class="col-md-12">
            	<div class="carousel-testimony owl-carousel">
                	<div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            	<div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_1.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Gabby Smith</p>
                                        <span class="position">Kepala Dusun Kraja</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            	<div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_2.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Floyd Weather</p>
                                        <span class="position">Kepala Dusun Curah Damar</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            	<div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_3.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">James Dee</p>
                                        <span class="position">Kepala Dusun Gunung Gumitir</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                            	<div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_1.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                            <i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Lance Roger</p>
                                        <span class="position">Kepala Dusun Tanah Manis</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_2.jpg')}})">
                                		<span class="quote d-flex align-items-center justify-content-center">
                                            <i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">john</p>
                                        <span class="position">Kepala Dusun Curah Manis</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_2.jpg')}})">
                                		<span class="quote d-flex align-items-center justify-content-center">
                                            <i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">john</p>
                                        <span class="position">Kepala Dusun Garahan Kidul</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-counter ftco-bg-dark " id="section-counter" style="background-image: url({{asset('assets/images/beranda/panorama.JPG')}});" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row justify-content-center">
        		<div class="col-md-10">
                	<div class="row">
                    	<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="11525">0</strong>
		              				<span>Jumlah Penduduk (Jiwa)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="3307">0</strong>
		              				<span>Jumlah Kepala Keluarga (KK)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="78">0</strong>
		              				<span>Jumlah RT</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="26 ">0</strong>
		              				<span>Jumlah RW</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>
@endsection
