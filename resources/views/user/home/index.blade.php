@extends('user.layouts.app')
@section('content')
<section class="home-slider js-fullheight owl-carousel ftco-degree-bottom">
	<div class="slider-item js-fullheight" style="background-image: url({{asset('assets/images/beranda/palkuning.JPG')}});">
    	<div class="overlay"></div>
        	<div class="container-fluid">
            	<!-- <div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">
                	<div class="col-sm-12 ftco-animate text-center mt-5 pt-md-5">
                    	<h1 class="mb-4">Crossfit is for<span>Everyone</span></h1>
                    	<h2 class="subheading">Shape your body</h2>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="slider-item js-fullheight" style="background-image: url({{asset('assets/images/beranda/panorama.JPG')}});">
      		<div class="overlay"></div>
            	<div class="container-fluid">
                	<div class="row slider-text js-fullheight justify-content-center align-items-center" data-scrollax-parent="true">
                    	<!-- <div class="col-sm-12 ftco-animate text-center mt-5 pt-md-5">
                        	<h1 class="mb-4">Crossfit<span>Gym</span></h1>
                        	<h2 class="subheading">Get Your Body Fit</h2>
                    	</div> -->
                    </div>
                </div>
            </div>
		</div>
	</div>
</section> 
<section class="ftco-section ftco-about">
	<div class="container">
    	<div class="row">
    		<div class="col-md-6">
            	<div class="img img-video d-flex align-items-center" style="background-image: url({{asset('assets/images/beranda/palkuning.JPG')}});">
    				<div class="video justify-content-center">
                		<a href="https://www.youtube.com/watch?v=7lP_f1YyDSw" class="icon-video popup-vimeo d-flex justify-content-center align-items-center">
							<span class="ion-ios-play"></span>
		  				</a>
					</div>
                </div>
                <div class="thumb-wrap d-flex">
    				<a class="thumb img" style="background-image: url({{asset('assets/images/beranda/peternakan1.JPG')}});"></a>
    				<a class="thumb img" style="background-image: url({{asset('assets/images/beranda/gangBali.JPG')}});"></a>
    				<a class="thumb img" style="background-image: url({{asset('assets/images/beranda/pabrikKopi.JPG')}});"></a>
                </div>
    		</div>
    		<div class="col-md-6 pl-md-5 ftco-animate d-flex align-items-center">
            	<div class="text pt-4 pt-md-0">
                	<div class="heading-section mb-4">
                        <h2 class="mb-1">Welcome to <span>Sidomulyo</span></h2>
                    </div>
                    <p>Desa Sidomulyo merupakan desa ke-9 di wilayah kecamatan Silo, merupakan desa pecahan dari Desa Garahan mulai tahun 1990 dan menjadi desa definitif pada tahun 1994. Desa Sidomulyo terletak pada ketinggian 560 m dari permukaan laut. Sepintas kondisi wilayah Desa Sidomulyo merupakan daerah pegunungan, dan sebagian besar terdiri dari tanah kering. Topografi desa ini terdiri atas dataran seluas 2357 hektar, serta pebukitan dan pegunungan seluas 2636 hektar. Desa Sidomulyo merupakan sentra tanaman perkebunan kopi, apokat dan petai. Curah hujan di Desa Sidomulyo cukup tinggi setiap tahunnya, yaitu 2000 ml pertahun. </p>
	          </div>
    		</div>
        </div>
    </div>
</section>
<section class="ftco-section bg-light">
	<div class="container">
    	<div class="row justify-content-center">
    		<div class="col-md-8 text-center">
            	<div class="heading-section mb-5">
                	<!-- <span class="subheading"><small><i class="left-bar"></i>Services<i class="right-bar"></i></small></span> -->
	            	<h2 class="mb-1">Potensi <span>Desa</span></h2>
	          	</div>
    		</div>
		</div>
		<br><br>
        <div class="row">
    		<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<!-- <div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-factory"></span>
					</div> -->
					<div class="text px-md-2">
                		<h3>1. Sektor Bidang Perdagangan dan Industri</h3>
                		<p>Sektor bidang perdagangan di Desa Sidomulyo sangat menunjang karena pasar desa berada di tengah tengah Desa Sidomulyo terletak di dusun krajan ada beberapa pengusaha tahu dan tempe yang pemasarannya sampai keluar Desa. Di dusun Krajan banyak peternak kambing dan pengusaha ayam potong.</p>
					</div>
            	</div>
    		</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<!-- <div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-gym"></span>
					</div> -->
					<div class="text px-md-2">
                		<h3>2. Sektor Bidang Pertanian</h3>
                		<p>Tanaman padi masih merupakan komoditas utama bidang pertanian disamping jagung, kedelai dan kacang tanah. Untuk buah-buahan, Desa Sidomulyo merupakan pemasok buah alpokat, kopi dan kelapa yang pemasarannya sampai ke Jakarta.</p>
					</div>
            	</div>
			</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<!-- <div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-tools-and-utensils"></span>
					</div> -->
					<div class="text px-md-2">
                		<h3>3. Sektor Bidang Perkebunan</h3>
                		<p>Di bidang perkebunan, yang cukup banyak dijumpai di wilayah ini adalah kelapa, kopi dan tembakau.</p>
					</div>
            	</div>
			</div>
			<div class="col-md-3">
            	<div class="services text-center ftco-animate">
					<!-- <div class="icon d-flex justify-content-center align-items-center">
                		<span class="flaticon-abs"></span>
					</div> -->
					<div class="text px-md-2">
                		<h3>4. Sektor Bidang Peternakan dan Perikanan</h3>
                		<p>Di bidang peternakan, ayam terutama ayam kampung merupakan salah satu hewan ternak yang banyak diminati sebagian besar masyarakat disamping kambing dan sapi.</p>
					</div>
            	</div>
			</div>
        </div>
    </div>
</section>
<section class="ftco-section testimony-section">
	<div class="container">
    	<div class="row justify-content-center mb-5 pb-3">
        	<div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-1">Perangkat Desa</h2>
            </div>
        </div>
        <div class="row ftco-animate">
        	<div class="col-md-12">
            	<div class="carousel-testimony owl-carousel">
                	<div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_1.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Gabby Smith</p>
                                        <span class="position">Customer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_2.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Floyd Weather</p>
                                        <span class="position">Customer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_3.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">James Dee</p>
                                        <span class="position">Customer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_1.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Lance Roger</p>
                                        <span class="position">Customer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                    	<div class="testimony-wrap p-4 pb-5">
                        	<div class="text">
                            	<p class="mb-4 pb-1 pl-4 line">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                                <div class="d-flex align-items-center">
                    				<div class="user-img" style="background-image: url({{asset('assets/images/person_2.jpg')}})">
                                    	<span class="quote d-flex align-items-center justify-content-center">
                                        	<i class="icon-quote-left"></i>
                                        </span>
                                    </div>
                                    <div class="ml-4">
		                  				<p class="name">Kenny Bufer</p>
                                        <span class="position">Customer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-package-program bg-darken">
	<div class="container-fluid px-0">
		<div class="row justify-content-center mb-5 pb-3">
        	<div class="col-md-7 heading-section heading-section-white ftco-animate text-center">
            	<span class="subheading"><small><i class="left-bar"></i>Sidomulyo<i class="right-bar"></i></small></span>
                <h2 class="mb-1">Recent <span>Activity</span></h2>
            </div>
        </div>
        <div class="row no-gutters">
        	<div class="col-md-12">
            	<div class="carousel-package-program owl-carousel">
					@foreach ($galery as $key => $galeryy)
                	<div class="item">
              			<div class="package-membership ftco-animate">
		        			<a href="#" class="img d-flex justify-content-center align-items-center" style="background-image: url({{asset('assets/images/galery/'.$galeryy->foto)}});"></a>
                        </div>
                    </div>
					@endforeach
                </div>
        	</div>
        </div>
    </div>
</section>
<!-- <section class="ftco-section bg-light">
	<div class="container">
    	<div class="row justify-content-center mb-5 pb-3">
        	<div class="col-md-7 heading-section ftco-animate text-center">
                <span class="subheading"><small><i class="left-bar"></i>Pricing Tables<i class="right-bar"></i></small></span>
                <h2 class="mb-1">Membership Plans</h2>
            </div>
        </div>
        <div class="row">
	    	<div class="col-md-4 ftco-animate">
	        	<div class="block-7">
	            	<div class="text-center">
	            		<h2 class="heading">One Day Training</h2>
						<span class="price"><sup>$</sup> <span class="number">7</span></span>
						<span class="excerpt d-block">100% free. Forever</span>
						<a href="#" class="btn btn-primary d-block px-2 py-4 mb-4">Get Started</a>						
	            		<h3 class="heading-2 mb-4">Enjoy All The Features</h3>
						<ul class="pricing-text">
							<li>Onetime Access To All Club</li>
							<li>Group Trainer</li>
							<li>Book A Group Class</li>
							<li>Fitness Orientation</li>
						</ul>
	            	</div>
	        	</div>
	        </div>
	        <div class="col-md-4 ftco-animate">
	        	<div class="block-7">
	        		<div class="text-center">
						<h2 class="heading">Pay Every Month</h2>
						<span class="price"><sup>$</sup> <span class="number">65</span></span>
						<span class="excerpt d-block">All features are included</span>
						<a href="#" class="btn btn-primary d-block px-3 py-4 mb-4">Get Started</a>
						<h3 class="heading-2 mb-4">Enjoy All The Features</h3>	
	            		<ul class="pricing-text">
							<li>Group Classes</li>
							<li>Discuss Fitness Goals</li>
							<li>Group Trainer</li>
							<li>Fitness Orientation</li>
						</ul>
	            	</div>
	          	</div>
	        </div>
	        <div class="col-md-4 ftco-animate">
	        	<div class="block-7">
					<div class="text-center">
						<h2 class="heading">1 Year Membership</h2>
						<span class="price"><sup>$</sup> <span class="number">125</span></span>
						<span class="excerpt d-block">All features are included</span>
						<a href="#" class="btn btn-primary d-block px-3 py-4 mb-4">Get Started</a>
						<h3 class="heading-2 mb-4">Enjoy All The Features</h3>
						<ul class="pricing-text">
							<li>Group Classes</li>
							<li>Discuss Fitness Goals</li>
							<li>Group Trainer</li>
							<li>Fitness Orientation</li>
						</ul>
					</div>
				</div>
	        </div>
	    </div>
	    <div class="row mt-5">
    		<div class="col-md-4">
            	<div class="services d-flex ftco-animate">
					<div class="icon-2 d-flex justify-content-center align-items-center">
                		<span class="flaticon-ruler"></span>
					</div>
					<div class="text px-md-2 pl-4">
                		<h3>1K+ Equipment</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
    		</div>
			<div class="col-md-4">
            	<div class="services d-flex ftco-animate">
					<div class="icon-2 d-flex justify-content-center align-items-center">
                		<span class="flaticon-gym"></span>
					</div>
					<div class="text px-md-2 pl-4">
                		<h3>Open 24/7</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
			</div>
			<div class="col-md-4">
            	<div class="services d-flex ftco-animate">
					<div class="icon-2 d-flex justify-content-center align-items-center">
                		<span class="flaticon-tools-and-utensils"></span>
					</div>
					<div class="text px-md-2 pl-4">
                		<h3>Food Supply</h3>
                		<p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
					</div>
            	</div>
			</div>
        </div>
    </div>
</section> -->
<!-- <section class="ftco-appointment">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row d-md-flex align-items-center">
    			<div class="col-md-6 d-flex align-self-stretch img" style="background-image: url({{asset('assets/images/about-3.jpg')}});"></div>
	    			<div class="col-md-6 appointment pl-md-5 py-md-5 ftco-animate">
						<h3 class="mb-3">Calculate Your BMI</h3>
						<form action="#" class="appointment-form">
							<div class="form-group">
								<label for="height">Height</label>
								<div class="d-flex">
		    						<input type="text" class="form-control mr-2" placeholder="feet">
		    						<input type="text" class="form-control ml-2" placeholder="inches">
                    			</div>
	    					</div>
	    					<div class="form-group">
                    			<div class="d-flex">
	    							<div class="mr-2">
                        				<label for="height">Weight</label>
                        				<input type="text" class="form-control" placeholder="lbs">
			    					</div>
		    						<div class="ml-2">
                        				<label for="height">Age</label>
                        				<input type="text" class="form-control" placeholder="yrs">
			    					</div>
                    			</div>
	    					</div>
	    					<div class="d-md-flex">
                            	<div class="form-group d-flex">
		            				<input type="submit" value="Calculate" class="btn btn-secondary py-3 px-4 mr-2">
                                	<input type="submit" value="BMI" class="btn btn-primary py-3 px-4 ml-2">
                            	</div>
	    					</div>
                		</form>
	    			</div>    			
            	</div>
    		</div>
		</div>
	</div>
</section> -->
<!-- <section class="ftco-section bg-light">
	<div class="container">
    	<div class="row justify-content-center mb-5 pb-3">
        	<div class="col-md-7 heading-section ftco-animate text-center">
            	<span class="subheading"><small><i class="left-bar"></i>Articles<i class="right-bar"></i></small></span>
                <h2 class="mb-1">Recent Blog</h2>
            </div>
        </div>
    	<div class="row d-flex">
        	<div class="col-md-4 d-flex ftco-animate">
          		<div class="blog-entry justify-content-end">
                	<a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_1.jpg')}}');"></a>
                    <div class="text p-4 float-right d-block">
              			<div class="meta">
                        	<div><a href="#">July 01, 2019</a></div>
                            <div><a href="#">Admin</a></div>
                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex ftco-animate">
          		<div class="blog-entry justify-content-end">
                	<a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_2.jpg')}}');"></a>
                    <div class="text p-4 float-right d-block">
              			<div class="meta">
                        	<div><a href="#">July 01, 2019</a></div>
                            <div><a href="#">Admin</a></div>
                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex ftco-animate">
          		<div class="blog-entry">
                	<a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_3.jpg')}}');"></a>
                	<div class="text p-4 float-right d-block">
              			<div class="meta">
                        	<div><a href="#">July 01, 2019</a></div>
                            <div><a href="#">Admin</a></div>
                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                        </div>
                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- <section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url({{asset('assets/images/bg_2.jpg')}});" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
    	<div class="container">
        	<div class="row justify-content-center">
        		<div class="col-md-10">
            		<div class="row">
                    	<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="5000">0</strong>
		              				<span>Happy Customers</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="4560">0</strong>
		              				<span>Perfect Bodies</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="570">0</strong>
		              				<span>Working Hours</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
                        	<div class="block-18 text-center">
                            	<div class="text">
		              				<strong class="number" data-number="900">0</strong>
		              				<span>Success Stories</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section> -->
@endsection