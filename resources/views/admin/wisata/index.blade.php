@extends('admin.layouts.app')
@section('content')
<div class="right_col" role="main">
    <div class="">
    	<div class="page-title">
        	<div class="title_left">
                <h3> Wisata Sidomulyo</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
            	<div class="x_panel">
                	<div class="x_title">
                    <button style="margin-left:970px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#tambah">Tambah</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form role="form" method="POST" action="{{route('wisataAdmin.store')}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Tambah Wisata</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="foto">Foto</label>
                                            <input type="file" class="form-control-file" id="foto" name="foto">
                                        </div>
                                        <div class="form-group">
                                            <label for="namaDusun">Nama Wisata</label>
                                            <input type="text" class="form-control" id="namaWisata" placeholder="Nama Dusun" name="namaWisata">
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsi">Deskripsi</label>
                                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
                                        </div>       
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                        @foreach ($wisata as $key => $wisataa)
                            <div class="col-md-55">
                                <div class="thumbnail">
                                    <div class="image view view-first">
                                        <img style="width: 100%; display: block;" src="{{asset('assets/images/wisata/'.$wisataa->foto)}}" alt="image" />
                                        <div class="mask">
                                            <p>{{ $wisataa->namaWisata }}</p>
                                            <div class="tools tools-bottom">
                                                <form action="{{route('wisataAdmin.delete', $wisataa->id)}}" method="POST" style="display: inline-block;">
                                                    {{ csrf_field() }}
                                                    <a data-toggle="modal" data-target="#ubah{{ $wisataa->id }}" class="btn btn-primary btn-xs">ubah</a>
                                                    <button class="btn btn-danger" onclick="return confirm('Delete?')">HAPUS</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <p>{{ $wisataa->deskripsi }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="ubah{{ $wisataa->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form role="form" method="POST" action="{{route('wisataAdmin.update',$wisataa->id)}}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Ubah Dusun</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="foto">Foto</label>
                                                    <input type="file" class="form-control-file" id="foto" name="foto">
                                                </div>
                                                <div class="form-group">
                                                    <label for="namaWisata">Nama Wisata</label>
                                                    <textarea class="form-control" id="namaWisata" name="namaWisata" rows="1" placeholder="Nama Dusun" required>{{ $wisataa->namaWisata }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="deskripsi">Deskripsi</label>
                                                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="Deskripsi" required>{{ $wisataa->deskripsi }}</textarea>
                                                </div>       
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        {{ $wisata->links() }}
                    </div>
        	    </div>
    	    </div>
        </div>
    </div>
</div>
@endsection