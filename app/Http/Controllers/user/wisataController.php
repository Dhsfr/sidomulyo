<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wisata;
use App\Repositories\WisataRepository;


class wisataController extends Controller{

    private $WisataRepository;

    public function __construct(WisataRepository $WisataRepository){

        $this->WisataRepository = $WisataRepository;

    }
  
    public function index(Request $request){
        $page = $request->get('page', 1);
        $limit = 10;

        $wisata = $this->WisataRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'wisata' => $wisata
        ];

        return view('user/wisata/index',$data);

    }

    public function create(){
        

    }

    public function store(Request $request){
        

    }

    public function show($id){
        

    }

    public function edit($id){
        

    }

    public function update(Request $request, $id){
        

    }

    public function destroy($id){
        
        
    }
}
