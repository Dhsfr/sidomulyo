<?php

namespace App\Repositories;

use App\Models\Wisata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WisataRepository{

    private $model;

    public function __construct(Wisata $model){

        $this->model = $model;

    }

    public function get($pagination = null, $with = null){
        $wisata = $this->model
            ->when($with, function ($query) use ($with) {
                return $query->with($with);
            });

        if ($pagination) {
            return $wisata->paginate(10);
        }

        return $wisata->get();
    }


}
