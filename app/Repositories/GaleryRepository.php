<?php

namespace App\Repositories;

use App\Models\Galery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GaleryRepository{

    private $model;

    public function __construct(Galery $model){

        $this->model = $model;

    }

    public function get($pagination = null, $with = null){
        $galery = $this->model
            ->when($with, function ($query) use ($with) {
                return $query->with($with);
            });

        if ($pagination) {
            return $galery->paginate(10);
        }

        return $galery->get();
    }


}
