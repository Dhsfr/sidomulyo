<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Dusun as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Dusun extends Model{
    protected $fillable = [
        'foto','namaDusun', 'deskripsi',
        
    ];
}
